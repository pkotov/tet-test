import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2StateDeclaration, UIRouterModule } from '@uirouter/angular';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { RatesModule } from 'features/rates/rates.module';
import { RatesFilterModule } from 'features/rates/filters/rates-filter.module';

import { RatesStateComponent } from './rates-state.component';


const states: Ng2StateDeclaration[] = [
  {
    name: 'main.rates',
    url: '/?edit',
    component: RatesStateComponent,
    params: {
      edit: {
        dynamic: true
      }
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    FontAwesomeModule,
    RatesModule,
    RatesFilterModule,
    UIRouterModule.forChild({states})
  ],
  declarations: [
    RatesStateComponent
  ]
})
export class RatesStateModule {}
