import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UiOnParamsChanged, UIRouterGlobals } from '@uirouter/angular';

import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons/faPencilAlt';

import { RateObject, RateResult, RatesService } from 'features/rates/rates.service';


@Component({
  selector: 'app-rates-state',
  templateUrl: './rates-state.component.html',
  styleUrls: ['../../../styles/table.scss', './rates-state.component.scss']
})
export class RatesStateComponent implements OnInit, UiOnParamsChanged {
  public exchangeRates: RateResult;
  public filterFields: any = {
    currency: new FormControl('EUR'),
    date: new FormControl((new Date).toISOString().substr(0, 10))
  };

  constructor(
    private library: FaIconLibrary,
    private uiRouterGlobals: UIRouterGlobals,
    private ratesService: RatesService
  ) {}

  public ngOnInit(): void {
    this.library.addIcons(faPencilAlt);

    this.getRates();

    if (this.uiRouterGlobals.params.edit) {
      this.uiOnParamsChanged({ edit: this.uiRouterGlobals.params.edit });
    }
  }

  public uiOnParamsChanged(newParams: { [p: string]: any }): void {
    if (!newParams.edit) {
      return this.ratesService.clearDialogState();
    }

    this.ratesService.openEditDialog(newParams.edit, this.exchangeRates)
      .then((updatedRate: RateObject) => {
        this.exchangeRates.rates[updatedRate.toRate] = parseFloat(updatedRate.rate);
      })
      .finally(() => {
        this.ratesService.clearDialogState();
      });
  }

  public changeFilters(filter: any): void {
    this.getRates(filter.currency, filter.date);
  }

  private getRates(currency?: string, date?: string): void {
    this.ratesService.getRates(currency, date)
      .then((rates: RateResult) => {
        this.exchangeRates = rates;
      });
  }
}
