import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { RateResult, RatesService } from 'features/rates/rates.service';


@Component({
  selector: 'app-history-state',
  templateUrl: './history-state.component.html',
  styleUrls: ['../../../styles/table.scss', './history-state.component.scss']
})
export class HistoryStateComponent implements OnInit {
  private currentDate: string = (new Date).toISOString().substr(0, 10);

  public exchangeRates: RateResult;
  public filterFields: any = {
    base: new FormControl('EUR'),
    symbols: new FormControl('USD,RUB'),
    start_at: new FormControl('2020-01-01'),
    end_at: new FormControl(this.currentDate)
  };

  public constructor(
    private ratesService: RatesService
  ) {}

  public ngOnInit(): void {
    this.getRates();
  }

  public changeFilters(filter: any): void {
    this.getRates(filter);
  }

  private getRates(filter?: any): void {
    this.ratesService.getHistoryRates(filter)
      .then((rates: RateResult) => {
        this.exchangeRates = rates;
      });
  }
}
