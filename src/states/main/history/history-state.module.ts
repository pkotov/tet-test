import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Ng2StateDeclaration, UIRouterModule } from '@uirouter/angular';

import { HistoryStateComponent } from './history-state.component';

import { RatesModule } from 'features/rates/rates.module';
import { RatesFilterModule } from 'features/rates/filters/rates-filter.module';

const states: Ng2StateDeclaration[] = [
  {
    name: 'main.history',
    url: '/history',
    component: HistoryStateComponent
  }
];


@NgModule({
  imports: [
    CommonModule,
    RatesModule,
    RatesFilterModule,
    UIRouterModule.forChild({states})
  ],
  declarations: [
    HistoryStateComponent
  ]
})
export class HistoryStateModule {}
