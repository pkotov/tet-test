import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Ng2StateDeclaration, UIRouterModule } from '@uirouter/angular';

import { MainStateComponent } from './main.component';


const states: Ng2StateDeclaration[] = [
  {
    name: 'main',
    abstract: true,
    component: MainStateComponent
  },
  {
    name: 'main.rates.**',
    url: '/',
    loadChildren: (): any => import('./rates/rates-state.module').then(m => m.RatesStateModule)
  },
  {
    name: 'main.history.**',
    url: '/',
    loadChildren: (): any => import('./history/history-state.module').then(m => m.HistoryStateModule)
  }
];


@NgModule({
  imports: [
    CommonModule,
    UIRouterModule.forChild({ states })
  ],
  declarations: [
    MainStateComponent
  ]
})
export class MainStateModule {}
