import { NgModule } from '@angular/core';

import { MainStateModule } from './main/main.module';


@NgModule({
  imports: [
    // Guards
    // If need to

    // Available Global States
    MainStateModule
  ]
})
export class StatesModule {}
