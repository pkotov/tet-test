import { Injectable } from '@angular/core';
import { StateService } from '@uirouter/core';
import { HttpClient } from '@angular/common/http';

import { DialogService } from 'common/dialog/dialog.service';

import { RatesDialogComponent } from './dialog/rates-dialog.component';

export interface RateResult {
  base: string;
  date: string;
  start_at: string;
  end_at: string;
  rates: {[key: string]: number};
}

export interface RateObject {
  toRate: string;
  rate: string;
}



@Injectable()
export class RatesService {
  private currentDate: string = (new Date).toISOString().substr(0, 10); // Date in format YYYY-MM-DD
  private cache: object;

  constructor(
    private httpClient: HttpClient,
    private stateService: StateService,
    private dialogService: DialogService
  ) {}

  public getRates(currency?: string, date?: string): Promise<RateResult> {
    const newDate: string = date || 'latest';
    const base: string = currency || 'EUR';
    return this.httpClient.get<RateResult>('https://api.exchangeratesapi.io/' + newDate, {
      params: { base }
    })
      .toPromise()
      .then((rateResult: RateResult) => this.updateRatesWithSavedData(rateResult));
  }

  public getHistoryRates(filter: any): Promise<any> {
    return this.httpClient.get<any>('https://api.exchangeratesapi.io/history', {
        params: {
          base: filter?.base || 'EUR',
          start_at: filter?.start_at || '2020-01-01',
          end_at: filter?.end_at || this.currentDate,
          symbols: filter?.symbols || 'USD,RUB'
        }
      })
      .toPromise();
  }

  public openEditDialog(rate: string, rates: RateResult): Promise<RateObject> {
    return this.dialogService.show(RatesDialogComponent, {
        clickToClose: true,
        escClose: true,
        hasBackdrop: true,
        inputs: {
          toRate: rate,
          base: rates.base
        }
      })
      .then((values: RateObject) => {
        this.saveCache(rates.base, values.toRate, values.rate);
        return {
          toRate: values.toRate,
          rate: values.rate
        };
      });
  }

  public clearDialogState(): void {
    this.stateService.go('.', { edit: null });
    this.dialogService.deattach();
  }

  private get cacheKey(): string {
    return 'rate-' + this.currentDate;
  }

  private updateRatesWithSavedData(rateResult: RateResult): RateResult {
    this.currentDate = rateResult.date;
    this.cache = this.readCache();

    const cache = this.cache[this.cacheKey] && this.cache[this.cacheKey][rateResult.base];
    if (cache) {
      const keys: string[] = Object.keys(cache);

      keys.forEach((key: string) => {
        rateResult.rates[key] = cache[key];
      });
    }

    return rateResult;
  }

  private readCache(): object {
    try {
      const storage: string = localStorage.getItem(this.cacheKey);
      return JSON.parse(storage) || {};
    } catch (e) {
      console.error('Could`t parse JSON', e);
      return {};
    }
  }

  private saveCache(base: string, valut: string, value: string): void {
    this.cache[this.cacheKey] = this.cache[this.cacheKey] || {};
    this.cache[this.cacheKey][base] = this.cache[this.cacheKey][base] || {};
    this.cache[this.cacheKey][base][valut] = value;
    localStorage.setItem(this.cacheKey, JSON.stringify(this.cache));
  }
}
