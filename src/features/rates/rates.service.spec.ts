import { TestBed } from '@angular/core/testing';
import { StateService } from '@uirouter/core';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { DialogService } from 'common/dialog/dialog.service';

import { RatesService } from './rates.service';


describe('RatesService', () => {
  let ratesService: RatesService;
  let httpTestingController: HttpTestingController;

  const stateService: StateService = jasmine.createSpyObj('StateService', ['go', 'show', 'detach']);
  const dialogService: DialogService = jasmine.createSpyObj('DialogService', ['show', 'detach']);


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        RatesService,
        { provide: StateService, useValue: stateService },
        { provide: DialogService, useValue: dialogService }
      ]
    });

    httpTestingController = TestBed.get(HttpTestingController);
    ratesService = TestBed.inject(RatesService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  describe('getRates', () => {
    it('Should make request with default getRates parameters and run updateRatesWithSavedData function`', (done: DoneFn) => {
      const updateRates = spyOn<any>(ratesService, 'updateRatesWithSavedData');
      const object: any = { test: 0 };

      ratesService.getRates()
        .then(() => {
          expect(updateRates).toHaveBeenCalledWith(object);
          done();
        });

      httpTestingController
        .expectOne('https://api.exchangeratesapi.io/latest?base=EUR')
        .flush(object);
    });

    it('Should work with optional parameters and make correct call', () => {
      ratesService.getRates('USD');
      ratesService.getRates('USD', '2020-01-01');

      httpTestingController.expectOne('https://api.exchangeratesapi.io/latest?base=USD');
      httpTestingController.expectOne('https://api.exchangeratesapi.io/2020-01-01?base=USD');
    });
  });
});
