import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { DialogModule } from 'common/dialog/dialog.module';
import { TextInputModule } from 'common/input/text-input/text-input.module';

import { RatesService } from './rates.service';
import { RatesDialogComponent } from './dialog/rates-dialog.component';


@NgModule({
  imports: [
    HttpClientModule,
    DialogModule,
    ReactiveFormsModule,
    TextInputModule
  ],
  providers: [
    RatesService
  ],
  declarations: [
    RatesDialogComponent
  ]
})
export class RatesModule {}
