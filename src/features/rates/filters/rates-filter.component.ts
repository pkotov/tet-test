import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-rates-filter',
  templateUrl: './rates-filter.component.html',
  styleUrls: ['./rates-filter.component.scss']
})
export class RatesFilterComponent {
  @Output() public change: EventEmitter<object> = new EventEmitter<object>();

  public form: FormGroup = new FormGroup({});

  @Input()
  public set fields(controls: { [key: string]: FormControl }) {
    const keys: string[] = Object.keys(controls);
    keys
      .forEach((key: string) => {
        this.form.addControl(key, controls[key]);
      });
  }

  public makeSubmit(event: Event): void {
    event.preventDefault();

    if (this.form.valid) {
      this.change.emit(this.form.value);
    }
  }
}
