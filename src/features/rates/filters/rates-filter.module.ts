import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TextInputModule } from 'common/input/text-input/text-input.module';

import { RatesFilterComponent } from './rates-filter.component';


@NgModule({
  imports: [
    TextInputModule,
    CommonModule
  ],
  declarations: [
    RatesFilterComponent
  ],
  exports: [
    RatesFilterComponent
  ]
})
export class RatesFilterModule {}
