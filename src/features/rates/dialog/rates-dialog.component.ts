import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { DialogRef } from 'common/dialog/dialog.ref';


@Component({
  selector: 'app-rates-dialog',
  templateUrl: './rates-dialog.component.html',
  styleUrls: ['./rates-dialog.component.scss']
})
export class RatesDialogComponent {
  @Input() public toRate: string;
  @Input() public base: string;

  public form: FormGroup = new FormGroup({
    rate: new FormControl(null, [Validators.required, Validators.pattern(/^\d+(\.\d+)?$/)])
  });

  public constructor(
    private dialogRef: DialogRef
  ) {}

  public saveForm(event: Event): void {
    event.preventDefault();

    if (this.form.valid) {
      this.dialogRef.close({
        toRate: this.toRate,
        rate: this.form.value.rate
      });
    }
  }
}
