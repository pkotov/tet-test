import { NgModule } from '@angular/core';

import { TextInputComponent } from './text-input.component';
import { ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";


@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule
  ],
  declarations: [
    TextInputComponent
  ],
  exports: [
    TextInputComponent
  ]
})
export class TextInputModule {}
