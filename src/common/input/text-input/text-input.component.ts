import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';


@Component({
  selector: 'app-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.scss']
})
export class TextInputComponent implements OnInit {
  @Input() public title: string;
  @Input() public control: FormControl;

  public id: string = Math.random().toString(36);

  public ngOnInit(): void {
  }
}
