import { Directive, HostListener, Input } from '@angular/core';

import { DialogRef } from './dialog.ref';


@Directive({
    selector: '[appDialogClose]'
})
export class DialogCloseDirective {
    @Input() public twinDialogClose: any;

    public constructor(
        private dialogRef: DialogRef
    ) {}

    @HostListener('click')
    public onClick(): void {
        this.dialogRef.close(this.twinDialogClose);
    }
}
