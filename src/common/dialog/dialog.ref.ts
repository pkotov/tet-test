import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';


@Injectable()
export class DialogRef {
    public readonly afterClose: Subject<any> = new Subject();

    public close(data?: any): void {
        this.afterClose.next(data);
        this.afterClose.complete();
    }

    public cancel(data?: any): void {
        this.afterClose.error(data);
    }
}
