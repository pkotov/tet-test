import { Directive, HostListener, Input } from '@angular/core';

import { DialogRef } from './dialog.ref';


@Directive({
    selector: '[appDialogCancel]'
})
export class DialogCancelDirective {
    @Input() public twinDialogCancel: any;

    public constructor(
        private dialogRef: DialogRef
    ) {}

    @HostListener('click')
    public onClick(): void {
        this.dialogRef.cancel(this.twinDialogCancel);
    }
}
