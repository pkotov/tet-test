import { ApplicationRef, ComponentFactory, ComponentFactoryResolver, ComponentRef, Injectable, Injector, TemplateRef, Type } from '@angular/core';

import { DialogComponent } from './dialog.component';
import { DialogInjector } from './dialog.injector';
import { DialogRef } from './dialog.ref';
import { DialogOptions, IDialogOptions } from './dialog.options';


@Injectable({
    providedIn: 'root'
})
export class DialogService {
    private componentRef: ComponentRef<any> = null;
    private contentRef: ComponentRef<any> = null;
    private dialogRef?: DialogRef;

    public constructor(
        private resolver: ComponentFactoryResolver,
        private injector: Injector,
        private appRef: ApplicationRef,
    ) {}

    public show<T = any>(component: Type<any>, options: Partial<IDialogOptions> = {}): Promise<T> {
        if (!this.componentRef) {
            return this.attach(component, options)
                .finally(() => {
                    return this.deattach();
                });
        } else {
            return this.deattach()
                .then(() => {
                    return this.attach(component, options);
                })
                .finally(() => {
                    return this.deattach();
                });
        }
    }

    private attach<T = any>(component: Type<any>, options: Partial<IDialogOptions>): Promise<T> {
        // Setup Injector
        const map: WeakMap<any, any> = new WeakMap();
        const dialogRef: DialogRef = new DialogRef();
        const dialogOptions: DialogOptions = new DialogOptions(options as IDialogOptions);
        map.set(DialogOptions, dialogOptions);
        map.set(DialogRef, dialogRef);
        const dialogInjector: DialogInjector = new DialogInjector(this.injector, map);

        // Create dialog
        const componentFactory: ComponentFactory<DialogComponent> = this.resolver.resolveComponentFactory(DialogComponent);
        const ngContent: any = this.resolveNgContent(component, dialogOptions.inputs, dialogInjector);
        const componentRef: ComponentRef<any> = this.componentRef = componentFactory.create(dialogInjector, ngContent);
        this.appRef.attachView(componentRef.hostView);
        document.body.appendChild(componentRef.location.nativeElement);

        this.dialogRef = dialogRef;

        return dialogRef.afterClose.toPromise();
    }

    public deattach(): Promise<void> {
        return new Promise((resolve: () => void): any => {
            if (this.dialogRef) {
                this.dialogRef.afterClose.error(null);
            }

            if (this.contentRef) {
                this.contentRef = void this.contentRef.destroy();
            }

            if (this.componentRef) {
                this.componentRef = void this.componentRef.destroy();
            }

            setTimeout(() => {
              // Add animations here if needed
              // --------
              // Call angular digest to clear app references before promise resolve
              resolve();
            }, 0);
        });
    }

    private resolveNgContent(content: any, options: any, dialogInjector: DialogInjector): any {
        if (typeof content === 'string') {
            return [[document.createTextNode(content)]];
        } else if (content instanceof TemplateRef) {
            return [content.createEmbeddedView(null).rootNodes];
        } else {
            const factory: ComponentFactory<any> = this.resolver.resolveComponentFactory(content);
            const componentRef: ComponentRef<any> = factory.create(dialogInjector);

            this.contentRef = componentRef;

            // Components inputs
            Object.keys(options).forEach((option: any) => componentRef.instance[option] = options[option]);

            this.appRef.attachView(componentRef.hostView);
            return [[componentRef.location.nativeElement]];
        }
    }
}
