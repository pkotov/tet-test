import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DialogCancelDirective } from './dialog-cancel.directive';
import { DialogCloseDirective } from './dialog-close.directive';
import { DialogComponent } from './dialog.component';
import { DialogService } from './dialog.service';


@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        DialogCloseDirective,
        DialogCancelDirective,
        DialogComponent
    ],
    providers: [
        // @forRoot() DialogService
    ],
    exports: [
        DialogCloseDirective,
        DialogCancelDirective
    ]
})
export class DialogModule {}
