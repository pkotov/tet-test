import { Component, HostListener } from '@angular/core';

import { DialogOptions } from './dialog.options';
import { DialogRef } from './dialog.ref';


@Component({
    selector: 'app-dialog',
    templateUrl: './dialog.component.html',
    styleUrls: ['./dialog.component.scss']
})
export class DialogComponent {
    public constructor(
        public dialogOptions: DialogOptions,
        public dialogRef: DialogRef
    ) {}

    @HostListener('document:keydown.escape')
    public clickOutside(): void {
        if (this.dialogOptions.escClose) {
            this.dialogRef.cancel();
        }
    }

    public clickToClose(): void {
        if (this.dialogOptions.clickToClose) {
            this.dialogRef.cancel();
        }
    }
}
