import { extend } from '@uirouter/core';


export interface IDialogOptions {
    hasBackdrop: boolean;
    clickToClose: boolean;
    escClose: boolean;
    event?: MouseEvent;
    inputs: {[key: string]: any};
}

export class DialogOptions implements IDialogOptions {
    public clickToClose: boolean = true;
    public escClose: boolean = true;
    public event: MouseEvent;
    public hasBackdrop: boolean = true;
    public inputs: {[key: string]: any} = {};

    public constructor(options: IDialogOptions) {
        extend(this, options);
    }
}
