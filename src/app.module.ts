import {NgModule} from '@angular/core';
import { UIRouterModule, UIView } from '@uirouter/angular';
import { BrowserModule } from '@angular/platform-browser';

import { StatesModule } from './states/states.module';


@NgModule({
  imports: [
    BrowserModule,
    StatesModule,
    UIRouterModule.forRoot()
  ],
  bootstrap: [
    UIView
  ]
})
export class AppModule {}
