# Installation and running
Instructions about installation and running are below (default angular project using angular-cli).

## About project
* There are 2 color patters taken from internet
  * Palette Grey - https://colorhunt.co/palette/201686
  * Palette Action - https://colorhunt.co/palette/196113
* No other frameworks are used
* Mobile view for a website (Device width is 500px)
* 2 application states
  * Exchange rates
  * History for base currency
* Re-used components
  * Rates filter
  * Text input
* In addition
  * There is a dialog that allow modifying exchange rates for spec. date.
    Dialog has reactive form with validations.
    It saves data in Local Storage.

## ToDo
* Dropdown with currencies
* Date picker
* And many other things that can be improved here, but we can discuss it later :)

## App
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.0.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

